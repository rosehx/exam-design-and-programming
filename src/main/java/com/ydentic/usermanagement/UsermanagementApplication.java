package com.ydentic.usermanagement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.ydentic.usermanagement" })
public class UsermanagementApplication implements CommandLineRunner {

	private final Log logger = LogFactory.getLog("Test User Management");

	public static void main(String[] args) {
		SpringApplication.run(UsermanagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("");
		logger.info("Application started: Test User Management");
		logger.info("Application running: http://localhost:8080/");
	}
}
