package com.ydentic.usermanagement;

import org.springframework.stereotype.Service;

import com.ydentic.usermanagement.database.JSONDatabase;
import com.ydentic.usermanagement.interfaces.IDatabase;
import com.ydentic.usermanagement.managers.EmployeeManager;

@Service
public class ManagerFactory {

	private IDatabase database;

	public ManagerFactory() {
		this.database = new JSONDatabase();
	}

	public EmployeeManager CreateEmployeeManager() {
		return new EmployeeManager(this.database);
	}

}
