package com.ydentic.usermanagement.interfaces;

import java.io.IOException;
import java.util.ArrayList;

public interface IDatabase {
	public ArrayList<IDatabaseEntity> Get(String tableName) throws IOException;

	public ArrayList<IDatabaseEntity> Set(ArrayList<IDatabaseEntity> entitiesToWrite, String tableName) throws IOException;

	public ArrayList<IDatabaseEntity> Put(ArrayList<IDatabaseEntity> entitiesToWrite, String tableName) throws IOException;

	public ArrayList<IDatabaseEntity> Pop(ArrayList<String> uniqueIds, String tableName) throws IOException;
}
