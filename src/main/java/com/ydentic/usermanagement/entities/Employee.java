package com.ydentic.usermanagement.entities;

import com.ydentic.usermanagement.interfaces.IDatabaseEntity;

public class Employee implements IDatabaseEntity {

	public String Id;

	public Boolean Active;

	public String Number;

	public String FirstName;

	public String LastName;

	public String PhoneNumber;

	public String Email;

	public String Manager;

	@Override
	public String GetUniqueId() {
		return this.Id;
	}

}
