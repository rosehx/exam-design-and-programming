package com.ydentic.usermanagement.controllers;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ydentic.usermanagement.ManagerFactory;
import com.ydentic.usermanagement.entities.Employee;
import com.ydentic.usermanagement.managers.EmployeeManager;

@RestController
public class EmployeeAPIController {

	public static final String PATH = "/employees";

	private EmployeeManager employeeManager;

	public EmployeeAPIController() {
		this.employeeManager = new ManagerFactory().CreateEmployeeManager();
	}

	@GetMapping(path = PATH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ArrayList<Employee>> GetEmployees(
			@RequestParam(name = "showInactive", defaultValue = "false") Boolean showInactive) {
		return this.MakeResponseObject(this.employeeManager.GetEmployees(showInactive), HttpStatus.OK);
	}

	@PutMapping(path = PATH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ArrayList<Employee>> AddEmployees(@RequestBody ArrayList<Employee> employees) {
		return this.MakeResponseObject(this.employeeManager.CreateEmployees(employees), HttpStatus.CREATED);
	}

	@PostMapping(path = PATH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ArrayList<Employee>> UpdateEmployees(@RequestBody ArrayList<Employee> employees) {
		return this.MakeResponseObject(this.employeeManager.UpdateEmployees(employees), HttpStatus.OK);
	}

	@DeleteMapping(path = PATH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> DeleteEmployees(@RequestParam ArrayList<String> employeeIds) {
		this.employeeManager.DeleteEmployees(employeeIds);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private ResponseEntity<ArrayList<Employee>> MakeResponseObject(ArrayList<Employee> employees,
			HttpStatus httpStatus) {
		return new ResponseEntity<ArrayList<Employee>>(employees, httpStatus);
	}
}
