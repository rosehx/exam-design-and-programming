package com.ydentic.usermanagement.managers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import com.ydentic.usermanagement.entities.Employee;
import com.ydentic.usermanagement.interfaces.IDatabase;
import com.ydentic.usermanagement.interfaces.IDatabaseEntity;

public class EmployeeManager {

	private IDatabase database;
	private final String databaseTable = "Employees";

	public EmployeeManager(IDatabase database) {
		this.database = database;
	}

	public ArrayList<Employee> GetEmployees(Boolean showInactive) {
		ArrayList<Employee> employees = new ArrayList<Employee>();

		try {
			ArrayList<IDatabaseEntity> storedEntities = this.database.Get(this.databaseTable);
			for (IDatabaseEntity entity : storedEntities) {
				Employee employee = (Employee) entity;
				if (!showInactive && (employee.Active == null || !employee.Active))
					continue;
				employees.add(employee);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return employees;
	}

	public ArrayList<Employee> CreateEmployees(ArrayList<Employee> employeesToCreate) {
		ArrayList<IDatabaseEntity> entities = new ArrayList<IDatabaseEntity>();
		for (Employee employee : employeesToCreate) {
			employee.Id = UUID.randomUUID().toString();
			employee.Active = true;
			entities.add(employee);
		}

		try {
			return DatabaseEntitiesToEmployees(this.database.Put(entities, this.databaseTable));
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<Employee>();
		}
	}

	public ArrayList<Employee> UpdateEmployees(ArrayList<Employee> employeesToUpdate) {
		ArrayList<IDatabaseEntity> entities = new ArrayList<IDatabaseEntity>();
		for (Employee employee : employeesToUpdate) {
//			if(employee.Active == null)
//				employee.
			entities.add(employee);
		}

		try {
			return DatabaseEntitiesToEmployees(this.database.Set(entities, this.databaseTable));
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<Employee>();
		}
	}

	public ArrayList<Employee> DeleteEmployees(ArrayList<String> employeesToDelete) {
		try {
			return DatabaseEntitiesToEmployees(this.database.Pop(employeesToDelete, this.databaseTable));
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<Employee>();
		}
	}

	private ArrayList<Employee> DatabaseEntitiesToEmployees(ArrayList<IDatabaseEntity> entities) {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		for (IDatabaseEntity entity : entities) {
			Employee employee = (Employee) entity;
			employees.add(employee);
		}
		return employees;
	}

}
