package com.ydentic.usermanagement.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ydentic.usermanagement.interfaces.IDatabase;
import com.ydentic.usermanagement.interfaces.IDatabaseEntity;

public class JSONDatabase implements IDatabase {

	private final String DATABASE_DIRECTORY = System.getProperty("user.home");
	
	private ArrayList<IDatabaseEntity> inMemoryEntities;

	public ArrayList<IDatabaseEntity> Get(String tableName) {
		if(this.inMemoryEntities != null)
			return this.inMemoryEntities;
		
		try {
			this.inMemoryEntities = this.Read(tableName);
			return this.inMemoryEntities;
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<IDatabaseEntity>();
		}
	}

	public ArrayList<IDatabaseEntity> Put(ArrayList<IDatabaseEntity> entitiesToWrite, String tableName)
			throws IOException {
		ArrayList<IDatabaseEntity> entitiesToStore = new ArrayList<IDatabaseEntity>(this.Read(tableName));
		entitiesToStore.addAll(entitiesToWrite);
		Write(entitiesToStore, tableName);
		return entitiesToWrite;
	}

	public ArrayList<IDatabaseEntity> Set(ArrayList<IDatabaseEntity> entitiesToWrite, String tableName)
			throws IOException {
		ArrayList<IDatabaseEntity> storedEntities = this.Read(tableName);

		HashMap<String, IDatabaseEntity> entitiesByUUID = new HashMap<String, IDatabaseEntity>();
		for (IDatabaseEntity storedEntity : storedEntities)
			entitiesByUUID.put(storedEntity.GetUniqueId(), storedEntity);

		ArrayList<IDatabaseEntity> updatebleEntities = new ArrayList<IDatabaseEntity>();
		for (IDatabaseEntity entity : entitiesToWrite) {
			final String entityId = entity.GetUniqueId();
			if (entitiesByUUID.containsKey(entityId))
				entitiesByUUID.put(entity.GetUniqueId(), entity);
			updatebleEntities.add(entity);
		}
		if (updatebleEntities.size() > 0) {
			Write(new ArrayList<IDatabaseEntity>(entitiesByUUID.values()), tableName);
		}

		return updatebleEntities;
	}

	public ArrayList<IDatabaseEntity> Pop(ArrayList<String> uniqueIds, String tableName) throws IOException {
		ArrayList<IDatabaseEntity> storedEntities;
		try {
			storedEntities = this.Read(tableName);
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<IDatabaseEntity>();
		}

		ArrayList<IDatabaseEntity> deletedEntities = new ArrayList<IDatabaseEntity>();
		Iterator<IDatabaseEntity> entityIterator = storedEntities.iterator();
		while (entityIterator.hasNext()) {
			IDatabaseEntity entity = entityIterator.next();
			if (uniqueIds.contains(entity.GetUniqueId())) {
				deletedEntities.add(entity);
				entityIterator.remove();
			}
		}
		if (deletedEntities.size() > 0)
			Write(storedEntities, tableName);
		return deletedEntities;
	}

	private void Write(ArrayList<IDatabaseEntity> entitiesToWrite, String tableName) throws IOException {
		final ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<Map<String, Object>> jsonObjects = new ArrayList<Map<String, Object>>();

		for (IDatabaseEntity entity : entitiesToWrite) {
			Map<String, Object> map = objectMapper.convertValue(entity, new TypeReference<Map<String, Object>>() {
			});
			map.put("#class", entity.getClass().getName());
			jsonObjects.add(map);
		}

		Path jsonFilePath = CreatePath(tableName);
		Path jsonFilePathParent = jsonFilePath.getParent();

		Files.deleteIfExists(jsonFilePath);

		if (!Files.exists(jsonFilePathParent))
			Files.createDirectories(jsonFilePathParent);

		Files.createFile(jsonFilePath);
		Files.write(CreatePath(tableName), objectMapper.writeValueAsBytes(jsonObjects));
		
		this.inMemoryEntities = entitiesToWrite;
	}

	private ArrayList<IDatabaseEntity> Read(String tableName) throws IOException {
		ArrayList<IDatabaseEntity> storedEntities = new ArrayList<IDatabaseEntity>();
		Path jsonFilePath = CreatePath(tableName);
		if (!Files.exists(jsonFilePath))
			return storedEntities;

		final ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = new String(Files.readAllBytes(jsonFilePath));

		ArrayList<Map<String, Object>> jsonObjects = objectMapper.readValue(jsonString,
				new TypeReference<ArrayList<Map<String, Object>>>() {
				});

		for (Map<String, Object> jObject : jsonObjects) {
			String cname = (String) jObject.remove("#class");

			try {
				storedEntities.add((IDatabaseEntity) objectMapper.convertValue(jObject, Class.forName(cname)));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		this.inMemoryEntities = storedEntities;
		return storedEntities;
	}

	private Path CreatePath(String tableName) {
		return Paths.get(this.DATABASE_DIRECTORY, "usermanager", "database", "json")
				.resolveSibling(tableName + ".json");
	}

}
